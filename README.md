Programming Problems
====================
Completed as part of a interview 2017
-------------------------------------

__Notes:__

Each problem is solved in it's own PHP file (problem_1.php, etc.)
located in the 'problems' directory.

Each problem has it's own simple test file (test_1.php, etc.)
which displays output for various inputs.


__Tools:__

- All coding, tests, and this text file were created on a Linux system,
so I cannot guarantee proper output formatting on Windows or Mac systems.
- Coding done in Sublime Text 3
- Tests ran at command line in xfce4-terminal
