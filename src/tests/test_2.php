<?php
include '../problems/problem_2.php';

echo 'to_roman function:' . "\n";
echo '1   : ' . to_roman(1) . "\n";
echo '5   : ' . to_roman(5) . "\n";
echo '6   : ' . to_roman(6) . "\n";
echo '80  : ' . to_roman(80) . "\n";
echo '499 : ' . to_roman(499) . "\n";
echo '1001: ' . to_roman(1001) . "\n";

echo "\n" . 'to_roman_custom function:' . "\n";
echo '1   : ' . to_roman_custom(1, '[', '%') . "\n";
echo '5   : ' . to_roman_custom(5, '[', '%') . "\n";
echo '6   : ' . to_roman_custom(6, '[', '%') . "\n";
echo '80  : ' . to_roman_custom(80, '[', '%') . "\n";
echo '499 : ' . to_roman_custom(499, '[', '%') . "\n";
echo '1001: ' . to_roman_custom(1001, '[', '%') . "\n";

echo "\n" . 'to_roman_float function:' . "\n";
echo '1.0      : ' . to_roman_float(1.0) . "\n";
echo '5.56     : ' . to_roman_float(5.56) . "\n";
echo '6.123    : ' . to_roman_float(6.123) . "\n";
echo '80.001   : ' . to_roman_float(80.001) . "\n";
echo '499      : ' . to_roman_float(499) . "\n";
echo '1001.1001: ' . to_roman_float(1001.1001) . "\n";