<?php
include '../problems/problem_3.php';

echo 'is_palindrome function:' . "\n";
echo 'a       : ' . is_palindrome('a') . "\n";
echo '123321  : ' . is_palindrome('123321') . "\n";
echo 'racecar : ' . is_palindrome('racecar') . "\n";
echo 'a man, a plan, a canal, panama : ' . is_palindrome('a man, a plan, a canal, panama') . "\n";
echo 'I saw desserts; I\'d no lemons; alas, no melon. Distressed was I. : ' . is_palindrome('I saw desserts; I\'d no lemons; alas, no melon. Distressed was I.') . "\n";

echo 'is_palindrome_alt function:' . "\n";
echo 'a       : ' . is_palindrome_alt('a') . "\n";
echo '123321  : ' . is_palindrome_alt('123321') . "\n";
echo 'racecar : ' . is_palindrome_alt('racecar') . "\n";
echo 'a man, a plan, a canal, panama : ' . is_palindrome_alt('a man, a plan, a canal, panama') . "\n";
echo 'I saw desserts; I\'d no lemons; alas, no melon. Distressed was I. : ' . is_palindrome_alt('I saw desserts; I\'d no lemons; alas, no melon. Distressed was I.') . "\n";



