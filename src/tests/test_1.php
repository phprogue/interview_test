<?php
include '../problems/problem_1.php';

echo 'atoi function:' . "\n";
echo '"123"       :' . atoi('123') . "\n";
echo '" 9999asdf" :' . atoi(' 9999asdf') . "\n";
echo '"2159 a"    :' . atoi('2159 a') . "\n";
echo '"-1"        :' . atoi('-1') . "\n";
echo '"  +123"    :' . atoi('  +123') . "\n";
echo '"abc"       :' . atoi('abc') . "\n";

echo "\n" . 'atoi_no_multi function:' . "\n";
echo '"123"       :' . atoi_no_multi('123') . "\n";
echo '" 9999asdf" :' . atoi_no_multi(' 9999asdf') . "\n";
echo '"2159 a"    :' . atoi_no_multi('2159 a') . "\n";
echo '"-1"        :' . atoi_no_multi('-1') . "\n";
echo '"  +123"    :' . atoi_no_multi('  +123') . "\n";
echo '"abc"       :' . atoi_no_multi('abc') . "\n";

echo "\n" . 'atoi_short function:' . "\n";
echo '"123"       :' . atoi_short('123') . "\n";
echo '" 9999asdf" :' . atoi_short(' 9999asdf') . "\n";
echo '"2159 a"    :' . atoi_short('2159 a') . "\n";
echo '"-1"        :' . atoi_short('-1') . "\n";
echo '"  +123"    :' . atoi_short('  +123') . "\n";
echo '"abc"       :' . atoi_short('abc') . "\n";
