<?php
include '../problems/problem_4.php';

echo 'ackermann function:' . "\n";
for ($m = 0; $m < 4; $m++) {
    for ($n = 0; $n < 4; $n++) {
        printf("(%d, %d) = %d\n", $m, $n, ackermann($m, $n));
    }
}

echo 'ackermann_cache function' . "\n";
for ($m = 0; $m < 4; $m++) {
    for ($n = 0; $n < 4; $n++) {
        printf("(%d, %d) = %d\n", $m, $n, ackermann_cache($m, $n));
    }
}

echo 'ackermann_optimized function' . "\n";
for ($m = 0; $m < 4; $m++) {
    for ($n = 0; $n < 4; $n++) {
        printf("(%d, %d) = %d\n", $m, $n, ackermann_cache($m, $n));
    }
}

echo 'ackermann_nonrecursive function' . "\n";
for ($m = 0; $m < 4; $m++) {
    for ($n = 0; $n < 4; $n++) {
        printf("(%d, %d) = %d\n", $m, $n, ackermann_nonrecursive($m, $n));
    }
}
