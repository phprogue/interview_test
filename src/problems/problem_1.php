<?php
/******************************************************************************

Programming Problems

Problem #1
atoi function

Completed by
Stephen Chatelain
stephen@hackworks.net

******************************************************************************/

/**
 * Problem 1
 *
 * Convert a string to integer
 * @param  string $str The string to be converted
 * @return int         The integer representation of the string
 */
function atoi($str)
{
    $result = 0;
    $idx = 0;
    $digit;
    $sign = 0;

    // Remove leading spaces
    while ($idx < strlen($str) && $str[$idx] == ' ') {
        $idx++;
    }
    // Check if $idx is out of bounds
    if ($idx == strlen($str))
        return $result;

    // Check for a sign
    if ($str[$idx] == '-') {
        $sign = 1;
        $idx++;
    } else {
        $sign = 0;
        if ($str[$idx] == '+') {
            $idx++;
        }
    }

    while ($idx < strlen($str)) {
        // working with ASCII values here
        $digit = ord($str[$idx]) - ord('0');
        if ($digit < 0 || $digit > 9) {
            break;
        }
        $result = ($result * 10) + $digit;
        $idx++;
    }

    if ($sign)
        return -$result;

    return $result;
}

/**
 * Problem 1b
 *
 * Convert a string to integer
 * @param  string $str The string to be converted
 * @return int         The integer representation of the string
 *
 */
function atoi_no_multi($str)
{
    $result = 0;
    $idx = 0;
    $digit;
    $sign = 0;

    // Remove leading spaces
    while ($idx < strlen($str) && $str[$idx] == ' ') {
        $idx++;
    }
    if ($idx == strlen($str))
        return $result;

    // Check for a sign
    if ($str[$idx] == '-') {
        $sign = 1;
        $idx++;
    } else {
        $sign = 0;
        if ($str[$idx] == '+') {
            $idx++;
        }
    }

    while ($idx < strlen($str)) {
        $digit = ord($str[$idx]) - ord('0');
        if ($digit < 0 || $digit > 9) {
            break;
        }
        // Using bit-shifting in place of multiplication to find result
        $result = (($result << 3) + ($result << 1)) + $digit;
        $idx++;
    }

    if ($sign)
        return -$result;

    return $result;
}

/**
 * Problem 1c
 *
 * Convert a string to integer
 * @param  string $str The string to be converted
 * @return int         The integer representation of the string
 *
 * Uses ternary operator in place of raw conditionals where appropriate
 */
function atoi_short($str)
{
    $result = 0;
    $idx = 0;
    $digit;
    $sign;

    // Remove leading spaces
    while ($idx < strlen($str) && $str[$idx] == ' ') {
        $idx++;
    }
    if ($idx == strlen($str))
        return $result;

    // Check for a sign
    $sign = ($str[$idx] == '-') ? 1 : 0;
    // If a sign exists as part of argument, increment index
    ($str[$idx] == '-' || $str[$idx] == '+') ? $idx++ : 0;

    while ($idx < strlen($str)) {
        $digit = ord($str[$idx]) - ord('0');
        if ($digit < 0 || $digit > 9) {
            break;
        }
        $result = ($result * 10) + $digit;
        $idx++;
    }

    return $sign ? -$result : $result;
}
