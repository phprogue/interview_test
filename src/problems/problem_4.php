<?php
/******************************************************************************

Programming Problems

Problem #4
Ackermann function

Completed by
Stephen Chatelain
stephen@hackworks.net

******************************************************************************/

/**
 * Problem 4
 *
 * Ackermann Implementation
 * @param  int $m
 * @param  int $n
 * @return int
 */
function ackermann($m, $n)
{
    // $m & $n >= 0
    if ($m < 0 || $n < 0)
        return 0;

    if ($m == 0) {
        return $n + 1;
    } else if ($n == 0) {
        return ackermann($m - 1, 1);
    }

    return ackermann($m - 1, ackermann($m, $n - 1));
}

/**
 * Problem 4b
 *
 * Ackermann Implementation that caches already-solved values
 * @param  int $m
 * @param  int $n
 * @return int
 */
function ackermann_cache($m, $n)
{
    // $m & $n >= 0
    if ($m < 0 || $n < 0)
        return 0;

    $cache = [];

    if (empty($cache[$m][$n])) {
        if ($m == 0) {
            $cache[$m][$n] = $n + 1;
        } else if ($n == 0) {
            $cache[$m][$n] = ackermann_cache($m - 1, 1);
        } else {
            $cache[$m][$n] = ackermann_cache($m - 1, ackermann_cache($m, $n - 1));
        }

    }

    return $cache[$m][$n];
}

/**
 * Problem 4c
 *
 * Ackermann implementation that optimizes low values of $m
 * @param  int $m
 * @param  int $n
 * @return int
 *
 * NOTE: This optimizes for m values <= 3, although calculating for m = 3
 * may not be much of an optimization since it uses exponents.
 * Also, I can't guarantee that it runs faster, but I believe
 * it would use less memory since it minimizes recursion.
 */
function ackermann_optimized($m, $n)
{
    // $m & $n >= 0
    if ($m < 0 || $n < 0)
        return 0;

    // return calculated answers for low $m values
    if ($m < 2)
        return $m + 1 + $n;
    if ($m == 2)
        return $m * $n + 3;
    if ($m == 3)
        return pow(2, $n + 3) - 3;

    if ($n == 0) {
        return ackermann($m - 1, 1);
    }

    return ackermann($m - 1, ackermann($m, $n - 1));
}

/**
 * Problem 4d
 *
 * Ackermann implementation without recursion
 * @param  int $m
 * @param  int $n
 * @return int
 *
 * NOTE: Since recursion simply uses the memory stack,
 * we can avoid recursion by creating our own stack.
 * (of course, I make it sound easier than it was since
 * before this problem, I didn't know what Ackermann's function was)
 */
function ackermann_nonrecursive($m, $n)
{
    // $m & $n >= 0
    if ($m < 0 || $n < 0)
        return 0;

    $stack[] = $m;
    while (count($stack) > 0) {
        $m = array_pop($stack);
        if ($m == 0) {
            $n += 1;
        } else if ($n == 0) {
            // Replace function call: ackermann($m - 1, 1);
            array_push($stack, --$m);
            $n++;
        } else {
            // Replace function call: ackermann($m - 1, ackermann($m, $n - 1))
            array_push($stack, --$m);
            array_push($stack, ++$m);
            $n--;
        }
    }

    return $n;
}
