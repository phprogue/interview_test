<?php
/******************************************************************************

Programming Problems

Problem #2
Integer to Roman numeral function

Completed by
Stephen Chatelain
stephen@hackworks.net

******************************************************************************/

/**
 * Problem 2
 *
 * Converts an integer to its Roman numeral equivalent
 * @param  int    $int An integer to convert
 * @return string      A string representing the Roman numeral
 */
function to_roman($int)
{
    $conversion = ['M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1];
    $result = '';
    while ($int > 0) {
        foreach ($conversion as $roman => $val) {
            if ($int >= $val) {
                $int -= $val;
                $result .= $roman;
                break;
            }
        }
    }

    return $result;
}

/**
 * Problem 2b
 *
 * Converts an integer to its Roman numeral equivalent
 * and substitutes Roman numerals 1 and 5 for passed arguments
 * @param  int    $int  An integer to convert
 * @param  char   $one  A replacement character for the Roman numeral I, optional
 * @param  char   $five A replacement character for the Roman numeral V, optional
 * @return string       A string representing the Roman numeral
 */
function to_roman_custom($int, $one = '', $five = '')
{
    // I realized after creating the $conversion array in the to_roman() function
    // that it's easier to manipulate the array if I reverse the key <-> value combinations
    $conversion = [1000=>'M', 900=>'CM', 500=>'D', 400=>'CD', 100=>'C', 90=>'XC', 50=>'L', 40=>'XL', 10=>'X', 9=>'IX', 5=>'V', 4=>'IV', 1=>'I'];
    $result = '';

    // Substitute existing numerals for user-defined symbols
    if (!empty($one)) {
        $conversion[1] = $one;
    }
    if (!empty($five)) {
        $conversion[5] = $five;
    }
    // update the $conversion array for numbers 4 & 9 with user-defined symbols
    $conversion[4] = $conversion[1] . $conversion[5];
    $conversion[9] = $conversion[1] . $conversion[10];

    while ($int > 0) {
        foreach ($conversion as $val => $roman) {
            if ($int >= $val) {
                $int -= $val;
                $result .= $roman;
                break;
            }
        }
    }

    return $result;
}

/**
 * Problem 2c
 *
 * Converts a float to its Roman numeral equivalent (if the Roman's used decimals ;-)
 * @param  float  $float A float to convert
 * @return string        A string representing the Roman numeral
 *
 * This function takes a float and breaks it into its integer and fractional parts.
 * From here, we can work on each part separately to convert to Roman numerals.
 * After that, we simply piece it back together with a decimal.
 * NOTE: Leading zeros of the fractional part are replaced with the letter 'Z'
 * in keeping with Roman numeral (capital letter) semantics
 */
function to_roman_float($float)
{
    $conversion = [1000=>'M', 900=>'CM', 500=>'D', 400=>'CD', 100=>'C', 90=>'XC', 50=>'L', 40=>'XL', 10=>'X', 9=>'IX', 5=>'V', 4=>'IV', 1=>'I'];
    $result = '';

    // Parse the float as if it were a string and operate on each part separately
    $parts = explode('.', $float);
    // We have to account for integers (no decimal)
    if (count($parts) == 1)
        $parts[1] = '0';

    // Get leading 0's of fractional part
    $fractionalLength = strlen($parts[1]);
    $parts[1] = ltrim($parts[1], '0');
    $leadingZeros = $fractionalLength - strlen($parts[1]);

    // Keep track of the integer/fractional part
    $fractional = false;
    foreach ($parts as $int) {
        $tmpResult = '';
        while ($int > 0) {
            foreach ($conversion as $val => $roman) {
                if ($int >= $val) {
                    $int -= $val;
                    $tmpResult .= $roman;
                    break;
                }
            }
        }
        // Add leading zeros back onto fractional part
        if ($fractional) {
            for ($i = 0; $i < $leadingZeros; $i++) {
                $tmpResult = 'Z' . $tmpResult;
            }
        }
        // Add the decimal back in
        $result .= $tmpResult;
        $result .= '.';
        $fractional = true;
    }
    // Remove the trailing decimal
    $result = substr($result, 0, -1);

    return $result;
}
