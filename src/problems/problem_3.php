<?php
/******************************************************************************

Programming Problems

Problem #3
Palindrome function

Completed by
Stephen Chatelain
stephen@hackworks.net

******************************************************************************/

/**
 * Problem 3
 *
 * Determines if a string is a palindrome
 * @param  string  $str The string to test
 * @return boolean      True if $str is a palindrome, false if not
 *
 * NOTE: This uses the builtin PHP function 'strrev'
 */
function is_palindrome($str)
{
    $result = false;
    // Strip out everything but letters and numbers
    $modStr = strtolower(preg_replace('/[^0-9a-zA-Z]/', '', $str));

    if ($modStr == strrev($modStr))
        $result = true;

    return $result;
}

/**
 * Problem 3 (alternate)
 *
 * Determines if a string is a palindrome
 * @param  string  $str The string to test
 * @return boolean      True if $str is a palindrome, fasle if not
 *
 * NOTE: This DOES NOT use the builtin PHP function 'strrev'
 * Also, it satisfies section 'b' and 'c' of the problem by
 * using constant memory and only accessing each letter once
 * (for odd-sized strings, it doesn't even access the middle letter)
 */
function is_palindrome_alt($str)
{
    $result = true;
    // Strip out everything but letters and numbers
    $modStr = strtolower(preg_replace('/[^0-9a-zA-Z]/', '', $str));

    $j = strlen($modStr);
    $halfSize = floor($j / 2);
    for ($i = 0; $i < $halfSize; $i++) {
        $j--;
        // Compare each side of the string
        // one character at a time,
        // working towards the center
        if ($modStr[$i] != $modStr[$j]) {
            $result = false;
            break;
        }
    }

    return $result;
}

